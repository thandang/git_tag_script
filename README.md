# README #



### What is this repository for? ###

* This is to help to create tag easily

### How do I get set up? ###

Pre-require: 

- Make sure that your local git is set properly on terminal

- Try to enter to right path where your project is located: cd /path to project/tag_push

- Make you checkout to right branch where the commit is located.

- Run command with following pattern:

sh tag_push.sh "<tag name>" "<commit id>" "<message>"

Sample: 

sh tag_push.sh "jul24_tag" "28e9b24e79893836f573bbf2dbe312e2ea8d84e7" "update new delivery"

sh tag_push.sh "jul24_tag" "6f2c6cb" "update new delivery"



### Who do I talk to? ###

* dtthan.infotrans@gmail.com