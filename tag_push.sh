#catching child's error exit
set -e

# Get user params
TAG_NAME="$1"
COMMIT_ID="$2"
MESSAGE="$3"

echo "---"
echo
echo "Fetch from remote - get all changes from remote"
echo
echo "---"

git remote update 

submitTag () {
    echo "---"
	echo
	echo "Preparing for create tag $TAG_NAME poit to commit $COMMIT_ID"
	echo
	echo "---"

	git tag -a "$TAG_NAME" "$COMMIT_ID" -m "$MESSAGE"
	git push origin --tags

	echo "---"
	echo
	echo "Done on tag $TAG_NAME"
	echo
	echo "---"
}

deleteTag() {
	echo "Git tag $TAG_NAME exists. perform delete"
    git tag -d "$TAG_NAME"
	git push origin ":$TAG_NAME"
}

echo "---"
echo
echo "Perform check tag existed with $TAG_NAME"
echo
echo "---"

if [ $(git tag -l "$TAG_NAME") ]; then
    deleteTag
    submitTag
else
    echo "tag $TAG_NAME not existed"
    submitTag
fi

